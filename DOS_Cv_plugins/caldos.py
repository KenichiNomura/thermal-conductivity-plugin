import os
import subprocess
import shlex
import matplotlib.pyplot as plt
import sys

def saveimage(filename,xlabel,ylabel,imagetitle,imagefile):
    inputdata = open(filename, 'r')
    xval=list();
    yval=list();
    for lines in inputdata:
        lines = lines.replace("\n", "").split()
        xval.append(float(lines[0]))
        yval.append(float(lines[1]))
    fig = plt.figure(figsize=(10,10))
    plt.rc('xtick', labelsize=20)
    plt.rc('ytick', labelsize=20)
    plt.rc('font', weight='bold')
    plt.plot(xval, yval, linewidth=3.5)
    plt.title(imagetitle)
    plt.xlabel(xlabel,fontsize=20, fontweight='bold')
    plt.ylabel(ylabel,fontsize=20, fontweight='bold')
    plt.xlim(xmin=0)
    plt.savefig(imagefile)
    plt.close()

#------main program---------

input="./dos"+" "+sys.argv[1]
os.system('rm -f dos')
os.system('gcc -lm -o dos dos.c')
args=shlex.split(input)
child_process=subprocess.Popen(args)
child_process.wait()
if child_process.returncode > 0 :
	print("-----error-------")
	sys.exit()
else:
	print("----Successful-----")
	#plot imags
	saveimage("DOS_total.txt","w(meV)","G(w)","Total Density of State","image/DOS_total.png")
	saveimage("PDOS_typeA.txt","w(meV)","G(w)","Partial Density of State atom 1","image/PDOS_typeA.png")
	saveimage("VOC_typeA.txt","time(ps)","<V(t)V(0)>/<V(0)V(0)>","Velocity auto-correlation atom 1","image/VOC_typeA.png")
	saveimage("PDOS_typeB.txt","w(meV)","G(w)","Partial Density of State atom 2","image/PDOS_typeB.png")
	saveimage("VOC_typeB.txt","time(ps)","<V(t)V(0)>/<V(0)V(0)>","Velocity auto-correlation atom 2","image/VOC_typeB.png")
	saveimage("Specific_heat.txt","Temperature","Cv","Specific heat","image/Specific_heat.png")
