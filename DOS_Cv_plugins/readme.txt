0. Prerequisit 

- LAMMPS 
LAMMPS executable is necessary to run obtain LAMMPS dump file for DOS calculation. If you do not have LAMMPS installed on your computer, 
please refer to LAMMPS website http://lammps.sandia.gov/download.html

- Python 3 and GCC
To calculate DOS and Cv from LAMMPS dump file, you will need Python 3 and gcc installed on your computer. 

Be sure you have Python version 3 like below.
> python --version 
Python 3.6.0 :: Anaconda 4.3.1 (64-bit)


1. DOS calculation
1.1 Run LAMMPS to create reaxed system
Run LAMMPS simulation to create the reaxled system for DOS and Cv calculatons. 

> mpirun -np Number_of_processor ./lammps_executable < in.dos 

NOTE: The name of LAMMPS executable, here lammps_executable, varies depending on your environment. If you do not know the name of LAMMPS executable 
on your computing environment, please contact to your administrators. 

After a sucessful run you will have a LAMMPS dump file, dumpdos.nve, which will be used for the DOS and Cv calculations next step. 
We also provide a sample LAMMPS dump file, dumpdos.nve, for a quick test. 


2. Analyssis of LAMMPS dump file to compute PDOS,DOS and Cv
Use python script caldos.py to calculate DOS and Cv,

> python caldos.py dumpdos.nve

NOTE: caldos.py takes a LAMMPS dump file as the first argument to compute partial DOS, DOS and Cv.


The various input paramters for caldos.py are defined in an input file, input.txt. For example, the number of initial conditions, 
correlation length, timestep are defined in the input file. Modify the parameters based on your requirement. 
