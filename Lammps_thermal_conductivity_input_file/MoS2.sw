# The Stillinger-Moeber parameters in metal units, for MoS2.
# these entries are in LAMMPS "metal" units:
#   epsilon = eV; sigma = Angstroms
#   other quantities are unitless

# format of a single entry (one or More lines):
#   element 1, element 2, element 3, 
#             epsilon, sigma,  a,      lambda,  gamma, costheta0,   A,    B,      p, q, tol

# MoS2 S-Mo-S, SMo2 S-Mo-S
Mo  S    S     1   0.3640   8.7660  12.1858  3.0300   0.13923   6.9530   485.33  4  0  0.0

# SMo2 Mo-S, SMo3 Mo-S-Mo
S   Mo   Mo    1   0.3640   8.7660  28.9547  6.5300   0.13923   6.9530   485.33  4  0  0.0

# zero contribution, put here just because of LAMMPS convention
Mo  Mo   Mo    1  0.5650   7.6400  0.0     0.0      0.0         4.7920   183.01  4  0  0.0
Mo  Mo   S     1  0.000    0.0000  0.0     0.0      0.0         0.0      0.0     0  0  0.0
Mo  S    Mo    1  0.000    0.0000  0.0     0.0      0.0         0.0      0.0     0  0  0.0
S   Mo   S     1  0.000    0.0000  0.0     0.0      0.0         0.0      0.0     0  0  0.0
S   S    Mo    1  0.000    0.0000  0.0     0.0      0.0         0.0      0.0     0  0  0.0
S   S    S     1  0.455    9.4870  0.0     0.0      0.0         1.2660   902.95  4  0  0.0
