import re
import matplotlib.pyplot as plt
import numpy as np
import sys

datapoint=dict()      # stores temperature vs Distance information
xlow=0.0
xhigh=0.0
syslen=0.0
splitpoint=0.0
splitrange=3.0     # how much data to want to remove while doing linear fit
hot=0.0
cold=0.0
shiftdist=50.0
hotshift=0.0
coldshift=0.0
Tslope=dict()
## define varible used in heat flux calculation
##normal 1500*750
height=3.5                       #normal-1500   without ripples its 3.5
width=(197.242-(-0.213933))     #normal-1500
energy=0.00431*60              # energy input in ev/ps

#------------------------------------------------------------------
Area=height*width    # Area in Amstrong^2
dtstep=0.001         # timestep in ps
delT=1000*dtstep     # how frequently heat is added in ps
heatflux=-energy/(2.0*Area*delT)    # ev/(ps*Amstrong^2)
evtoWatt=1.60217733*1000        #10^-19/(10^-12*10^-10)

def readdata(filename,datapoint):
    inputdata=open(filename,'r')
    count=0
    count1=0
    timestep=0
    timeval=0
    npoint=0
    for line  in inputdata:
        count+=1
        line = line.replace("\n", "")
        if count > 3:
            count1+=1
            if count1==1:
                val=re.findall(r'\S+',line)
                tempstep=int(val[0])
                npoint=int(val[1])
                timeval+=1
                datapoint[timeval]=list()
                print(tempstep,npoint,timeval)
            elif count1<=1+npoint and count1>1:
                val = re.findall(r'\S+',line)
                templist=list()
                templist.append(float(val[1]))
                templist.append(float(val[3]))
                datapoint[timeval].append(templist)
            if count1==npoint+1: count1=0
    return

def printdata(datapoint):
    for keys in datapoint:
        print("Point for ",keys,'total points',len(datapoint[keys]))
        for values in datapoint[keys]:
            print(keys, values)
    return

def findboxsize(mydata):
    xmin=mydata[0]
    xhigh=mydata[len(mydata)-1]
    return xmin[0],xhigh[0]

def plotdata(mydata,xxmin,xxmax,timestep,Tslope):

    x1=list()
    y1=list()
    x2=list()
    y2=list()

    for val in mydata:
        x1.append(val[0])
        y1.append(val[1])
        if val[0] >=xxmin and val[0] <= xxmax:
            x2.append(val[0])
            y2.append(val[1])
    fit=np.polyfit(x2,y2,1)
    fit_fn=np.poly1d(fit)
    Tslope[timestep]=fit
#    print "slope and constant",fit[0],fit[1]
#    print x2[10],y2[10],fit_fn(x2[10]),fit[0]*x2[10]+fit[1]
    plt.plot(x1,y1,linewidth=2.0)
    plt.plot(x2, y2,linewidth=3.0)
#    plt.plot(x2,y2,'yo',x2,fit_fn(x2),"--k",linewidth=2.0)
    plt.title("timestep "+str(timestep*4)+"ns",color='r')
    plt.ylabel("Temperarature (K)")
    plt.xlabel("Distance (A)")
    plt.savefig("timestep"+str(timestep*4)+"ns.png")
    plt.close()
#    plt.show()
    return

# Program  Starts from here
filename=sys.argv[1]
readdata(filename,datapoint)
#printdata(datapoint)
xlow,xhigh=findboxsize(datapoint[1])
print("xlow and xhigh:",xlow,xhigh)
syslen=xhigh-xlow
splitpoint=syslen/4.0
hot=xlow+splitpoint
cold=xhigh-splitpoint
shiftdist=splitpoint/splitrange
hotshift=hot+shiftdist
coldshift=cold-shiftdist
print("splitpoint",splitpoint,"shiftdist",shiftdist)
print("hot",hot,"cold",cold)
print("hot",hotshift,"cold",coldshift)

for keys in datapoint:
    plotdata(datapoint[keys],hotshift,coldshift,keys,Tslope)

# Print  dT/dx value   in K/Amstrong
# unit of Conductivity is ev/(K*ps*Amstrong)
Kval=list()
Kinverse=list()
print("Heat flux added: ",heatflux)
count=0
for keys in Tslope:
    count+=1
    Kvalue=evtoWatt*heatflux/Tslope[keys][0]
    if count >=3:
        Kval.append(Kvalue)
        Kinverse.append(1/Kvalue)
    print("conductivty and slope : ",Kvalue,-Tslope[keys][0])
#    print keys,Tslope[keys]

#print("L 1/L Kval Kval_std Kinv Kinv_std")
print("Length:",0.05*syslen)
print("1/Length",20.0/syslen)
print("Mean Thermal Conductivty value and standard deviation",np.mean(Kval),np.std(Kval))
print("1/(Thermal_Conductivty) value and standard deviation",np.mean(Kinverse),np.std(Kinverse))
